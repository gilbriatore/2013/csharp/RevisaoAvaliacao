﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Negocios;
using DAOs;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            PessoaDAO dao = new PessoaDAO();
            List<Pessoa> lista = dao.getListaDePessoas();

            foreach (Pessoa pessoa in lista)
            {
                if (pessoa.isMenorDeIdade())
                {

                    Console.WriteLine("Nome: " + pessoa.Nome);
                    Console.WriteLine("Sobrenome: " + pessoa.Sobrenome);
                    Console.WriteLine("CPF: " + pessoa.Cpf);
                    Console.WriteLine("Data de nascimento: " + pessoa.DataDeNascimento);
                    Console.WriteLine("Idade: " + pessoa.getIdade());
                    //if (pessoa.isMenorDeIdade())
                    //{
                    //    Console.WriteLine("É menor: Sim");
                    //}
                    //else
                    //{
                    //    Console.WriteLine("É menor: Não");
                    //}
                }
                Console.WriteLine("-------------------------------");
            }

            Console.ReadLine();
        }
    }
}
