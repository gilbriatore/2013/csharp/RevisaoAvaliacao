﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Telas
{
    public partial class TelaDeDetalhes : Form
    {
        private Negocios.Pessoa p;

        public TelaDeDetalhes()
        {
            InitializeComponent();
        }

        public TelaDeDetalhes(Negocios.Pessoa p)
        {
            InitializeComponent();
            this.p = p;
            textBox1.Text = p.Nome;
            textBox2.Text = p.Sobrenome;
            textBox3.Text = p.Cpf;
            textBox4.Text = p.DataDeNascimento;
        }
    }
}
