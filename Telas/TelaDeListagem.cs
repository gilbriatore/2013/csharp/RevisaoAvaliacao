﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DAOs;
using Negocios;

namespace Telas
{
    public partial class TelaDeListagem : Form
    {
        public TelaDeListagem()
        {
            InitializeComponent();

            PessoaDAO dao = new PessoaDAO();
            List<Pessoa> lista = dao.getListaDePessoas();
            dgvPessoas.DataSource = lista;
        }

        private void dgvPessoas_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dgvPessoas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Pessoa p = (Pessoa)dgvPessoas.Rows[e.RowIndex].DataBoundItem;
            TelaDeDetalhes tela = new TelaDeDetalhes(p);
            tela.Show();
        }
    }
}
