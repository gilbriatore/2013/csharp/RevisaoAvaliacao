﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negocios
{
    public class Pessoa
    {
        private string nome;

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        private string sobrenome;

        public string Sobrenome
        {
            get { return sobrenome; }
            set { sobrenome = value; }
        }

        private string cpf;

        public string Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        }

        private string dataDeNascimento;

        public string DataDeNascimento
        {
            get { return dataDeNascimento; }
            set { dataDeNascimento = value; }
        }


        public Pessoa(string nome, string sobrenome, string cpf, string data)
        {            
            this.nome = nome;
            this.sobrenome = sobrenome;
            this.cpf = cpf;
            this.dataDeNascimento = data;
        }

        public bool isMenorDeIdade()
        {
            DateTime dataAtual = DateTime.Now;
            DateTime dataNascimento = Convert.ToDateTime(dataDeNascimento); 
            bool isMenor = dataNascimento.Year + 18 > dataAtual.Year;
            return isMenor;
        }

        public int getIdade()
        {
            DateTime dataAtual = DateTime.Now;
            DateTime dataNascimento = Convert.ToDateTime(dataDeNascimento);
            int idade = dataAtual.Year - dataNascimento.Year;
            return idade;

        }

    }
}
