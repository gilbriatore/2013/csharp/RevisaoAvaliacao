﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Negocios;

namespace DAOs
{
    public class PessoaDAO
    {
        private List<Pessoa> lista = new List<Pessoa>();

        public PessoaDAO()
        {
            string[] linhas = File.ReadAllLines("confidencial.txt");
            foreach (var linhaCriptografada in linhas)
            { 
                string linhaLegivel = Util.Criptografia.Descriptografar(linhaCriptografada);
                string[] dados = linhaLegivel.Split(';');
                Pessoa pessoa = new Pessoa(dados[0], dados[1], dados[2], dados[3]);
                lista.Add(pessoa);
            }
        }

        public List<Pessoa> getListaDePessoas()
        {
            return lista;
        }

    }
}
